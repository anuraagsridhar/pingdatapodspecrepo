Pod::Spec.new do |s|
    s.name              = 'PingDataSDK'
    s.version           = '1.0.2'
    s.summary           = 'PingData OCR Uplifting SDK.'
    s.homepage          = 'http://pingdata.io/'

    s.author            = { 'Name' => 'anu@helixta.com.au' }
    s.license           = { :type => 'BSD', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => 'https://bitbucket.org/anuraagsridhar/pingdatasdk_ios.git', :tag => "#{s.version}" }

    s.ios.deployment_target = '11.0'
    s.ios.vendored_frameworks = 'PingDataSDK.framework'
end
